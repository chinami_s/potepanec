require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "related_products" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create_list(:product, 4, taxons: [taxon]) }
    let!(:unrelated_product) { create(:product) }

    it "gets related_products with the same taxon as a product" do
      expect(product.related_products).to eq related_product
    end

    it "does not include the product it self" do
      expect(product.related_products).not_to include product
    end

    it "does not include the product with the other taxon" do
      expect(product.related_products).not_to include unrelated_product
    end
  end
end
