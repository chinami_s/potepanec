require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxons) { product.taxons }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    # 正常にレスポンスを返す
    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    # showページを正常に読み込んでいる
    it 'render show templete' do
      expect(response).to render_template :show
    end

    # インスタンス変数@productが存在する
    it '@product is present' do
      expect(assigns(:product)).to eq product
    end

    # インスタンス変数@taxonが存在する
    it '@taxon is present' do
      expect(assigns(:taxon)).to eq taxons.first
    end

    it '＠related_products is limited 4 products' do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
