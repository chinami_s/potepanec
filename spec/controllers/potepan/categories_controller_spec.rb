require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe '#show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    # 正常にレスポンスを返す
    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    # showページを正常に読み込んでいる
    it 'render show templete' do
      expect(response).to render_template :show
    end

    # インスタンス変数が存在する
    it '＠taxon is present' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '＠products is present' do
      expect(assigns(:products)).to match_array(product)
    end

    it '＠taxonomies is present' do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end
  end
end
