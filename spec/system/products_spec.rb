require 'rails_helper'

RSpec.describe 'Products', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  it "show a correct title" do
    expect(page).to have_title full_title(product.name)
  end

  it "show a correct product's name" do
    expect(page).to have_content product.name
  end

  it "show a correct product's price" do
    expect(page).to have_content product.price
  end

  it "show a correct product's description" do
    expect(page).to have_content product.description
  end

  it "exists a correct path to home" do
    expect(page).to have_link "Home", href: potepan_path
  end

  it "exists a correct path to category page" do
    expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(taxon.id)
  end

  it "can back to category page from product page", js: true do
    click_on "一覧ページへ戻る"
    expect(page).to have_current_path potepan_category_path(taxon.id)
  end

  it "shows up to 4 products with the same taxon" do
    within(".productsContent") do
      expect(page).to have_content related_products.first.name
      expect(page).to have_content related_products.first.display_amount
      expect(page).not_to have_content related_products.last.name
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content product.name
    end
  end

  it "can move to correct related product page" do
    within(".productsContent") do
      click_on related_products.first.name
    end
    expect(page).to have_current_path potepan_product_path(related_products.first.id)
    expect(page).to have_content related_products.first.name
    expect(page).to have_content related_products.first.display_amount
    expect(page).to have_content related_products.first.description
  end
end
