require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let!(:taxon1) { create(:taxon, name: 'Bags', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:taxon2) { create(:taxon, name: 'Mags', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product1) { create(:product, name: 'testBag', taxons: [taxon1]) }
  let!(:product2) { create(:product, name: 'testMag', taxons: [taxon2]) }

  before do
    visit potepan_category_path(taxon1.id)
  end

  it "show a correct categorypage" do
    expect(page).to have_title full_title(taxon1.name)
    expect(page).to have_content taxon1.name
    expect(page).to have_content taxonomy.name
    expect(page).to have_link 'Home', href: potepan_path
    expect(page).to have_content product1.name
    expect(page).to have_content product1.display_amount
  end

  it "has correct taxons if click on taxonomy", js: true do
    within("#category") do
      click_on taxonomy.name
      expect(page).to have_content "#{taxon1.name}(#{taxon1.all_products.count})"
      expect(page).to have_content "#{taxon2.name}(#{taxon2.all_products.count})"
    end
  end

  it "has no taxons unless click on taxonomy", js: true do
    within("#category") do
      expect(page).not_to have_content "#{taxon1.name}(#{taxon1.all_products.count})"
      expect(page).not_to have_content "#{taxon2.name}(#{taxon2.all_products.count})"
    end
  end

  it "move to correct category page", js: true do
    within("#category") do
      click_on taxonomy.name
      click_on taxon2.name
    end
    expect(page).to have_title full_title(taxon2.name)
    expect(page).to have_current_path potepan_category_path(taxon2.id)
  end

  it "show correct product after move to category page" do
    within("#category") do
      click_on taxonomy.name
      click_on taxon2.name
    end
    within(".categories-products") do
      expect(page).to have_content product2.name
      expect(page).to have_content product2.display_amount
    end
  end

  it "can move to correct product page" do
    click_on product1.name
    expect(page).to have_current_path potepan_product_path(product1.id)
    expect(page).to have_content product1.name
    expect(page).to have_content product1.display_amount
    expect(page).to have_content product1.description
    expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(taxon1.id)
  end
end
