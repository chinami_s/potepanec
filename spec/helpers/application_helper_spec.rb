require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it "return only base_title if nil" do
      expect(full_title(nil)).to eq 'BIGBAG Store'
    end

    it "return only base_title if empty" do
      expect(full_title('')).to eq 'BIGBAG Store'
    end

    it "return correct full_title if page_title is present" do
      expect(full_title('Test')).to eq 'Test - BIGBAG Store'
    end
  end
end
