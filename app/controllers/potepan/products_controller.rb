class Potepan::ProductsController < ApplicationController
  MAX_OF_DISPLAY_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.first
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).
      limit(MAX_OF_DISPLAY_RELATED_PRODUCTS)
  end
end
